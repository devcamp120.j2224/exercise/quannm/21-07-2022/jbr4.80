package com.devcamp.jbr480.jbr480.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.jbr480.jbr480.model.Circle;
import com.devcamp.jbr480.jbr480.model.Rectangle;
import com.devcamp.jbr480.jbr480.model.Square;

@RestController
public class ApiController {
    @CrossOrigin
    @GetMapping("/circle-area")
    public double getAreaCircle(@RequestParam double radius) {
        Circle circle = new Circle(radius);
        return circle.getArea();
    }

    @CrossOrigin
    @GetMapping("/circle-perimeter")
    public double getPerimeterCircle(@RequestParam double radius) {
        Circle circle = new Circle(radius);
        return circle.getPerimeter();
    }

    @CrossOrigin
    @GetMapping("/rectangle-area")
    public double getAreaRectangle(@RequestParam double width, double length) {
        Rectangle rectangle = new Rectangle(width, length);
        return rectangle.getArea();
    }

    @CrossOrigin
    @GetMapping("/rectangle-perimeter")
    public double getPerimeterRectangle(@RequestParam double width, double length) {
        Rectangle rectangle = new Rectangle(width, length);
        return rectangle.getPerimeter();
    }

    @CrossOrigin
    @GetMapping("/square-area")
    public double getAreaSquare(@RequestParam double side) {
        Square square = new Square(side);
        return square.getArea();
    }

    @CrossOrigin
    @GetMapping("/square-perimeter")
    public double getPerimeterSquare(@RequestParam double side) {
        Square square = new Square(side);
        return square.getPerimeter();
    }
}
